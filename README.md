# Task-1

This is warmup task, you will be doing string parser which will be working as calculator.

It should be ready to accept any input, and detect wrong one.

Example good input:

```bash
> 3+4+65-123*32 /12+54 + 65
```

Example bad input:

```bash
> 3+asdasdadsa-13
```

After calcualting it should write answer,

If calculation had good input, it should start with `>`,
If calculation had wrong input, it should start with `?`,
followed by answer (in case of bad input `WRONG INPUT`)

FE:

```bash
    > 3+3
    > 6
    > 3+x
    ? WRONG INPUT
    > 
```
